const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,varsta VARCHAR(3),cnp VARCHAR(20),sex VARCHAR(20))";
    connection.query(sql, function (err, result) {
      if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let birth_year = 0;
    let birth_month = 0;
    let birth_day = 0;
    var error = [];
    let sex = [];

    function calculate_age(birth_month,birth_day,birth_year)
    {
      today_date = new Date();
      today_year = today_date.getFullYear();
      today_month = today_date.getMonth();
      today_day = today_date.getDate();
      age = today_year - birth_year;

      if ( today_month < (birth_month - 1))
      {
          age--;
      }
      if (((birth_month - 1) == today_month) && (today_day < birth_day))
      {
          age--;
      }
      return age;
    }

    if (!nume || !prenume || !telefon || !email || !facebook || !tipAbonament || !nrCard || !cvv || !varsta || !cnp) {
      error.push("Unul sau mai multe campuri nu au fost introduse");
      console.log("Unul sau mai multe campuri nu au fost introduse!");
    } else {
      if (nume.length < 2 || nume.length > 30) {
        console.log("Nume invalid!");
        error.push("Nume invalid");
      } else if (!nume.match("^[A-Za-z]+$")) {
        console.log("Numele trebuie sa contina doar litere!");
        error.push("Numele trebuie sa contina doar litere!");
      }
    }
    if (prenume.length < 2 || prenume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!prenume.match("^[A-Za-z]+$") || !prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!email.includes("@gmail.com") && !email.includes("@yahoo.com") && !email.includes("@rocketmail.com") && !email.includes("@facebook.com")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }

    if (!facebook.includes("https://www.facebook.com/")) {
      console.log("Facebook invalid!");
      error.push("Facebook invalid!");
    }
    if (nrCard.length != 16) {
      console.log("Numarul cardului trebuie sa fie de 16 cifre!");
      error.push("Numarul cardului trebuie sa fie de 16 cifre!");
    } else if (!nrCard.match("^[0-9]+$")) {
      console.log("Numarul cardului trebuie sa contina doar cifre!");
      error.push("Numarul cardului trebuie sa contina doar cifre!");
    }
    if (cvv.length != 3) {
      console.log("CVV-ul trebuie sa fie format din 3 cifre!");
      error.push("CVV-ul trebuie sa fie format din 3 cifre!");
    } else if (!cvv.match("^[0-9]+$")) {
      console.log("CVV-ul trebuie sa fie format doar din cifre!");
      error.push("CVV-ul trebuie sa fie format doar din cifre!");
    }
    if (varsta.length < 1 || varsta.length > 3) {
      console.log("Varsta trebuie sa aibe minim o cifra!");
      error.push("Varsta trebuie sa aibe minim o cifra!");
    } else if (!varsta.match("^[0-9]+$")) {
      console.log("Varsta trebuie sa fie formata doar din cifre!");
      error.push("Varsta trebuie sa fie formata doar din cifre!");
    }
    if (cnp.length != 13) {
      console.log("CNP-ul trebuie sa fie de 13 cifre!");
      error.push("CNP-ul trebuie sa fie de 13 cifre!");
    } else if (!cnp.match("^[0-9]+$")) {
      console.log("CNP-ul trebuie sa contina doar cifre!");
      error.push("CNP-ul trebuie sa contina doar cifre!");
    } else {
      if (cnp[0] == 1 || cnp[0] == 2) {
        birth_year = 1900 + +cnp[1]*10 + +cnp[2];
        birth_month = +cnp[3]*10 + +cnp[4];
        birth_day = +cnp[5]*10 + +cnp[6];
      }
      else if (cnp[0] == 5 || cnp[0] == 6 || cnp[0] == 7 || cnp[0] == 8) {
        birth_year = 2000 + +cnp[1]*10 + +cnp[2];
        birth_month = +cnp[3]*10 + +cnp[4];
        birth_day = +cnp[5]*10 + +cnp[6];
      } else {
        console.log("CNP Invalid");
        error.push("CNP Invalid");
      }
    }

    if (+cnp[0] == 1 || +cnp[0] == 3 || +cnp[0] == 5 || +cnp[0] == 7) sex.push("Masculin");

    if (+cnp[0] == 2 || +cnp[0] == 4 || +cnp[0] == 6 || +cnp[0] == 8) sex.push("Feminin");

    if (+varsta !== +calculate_age(birth_month,birth_day,birth_year)) error.push("Varsta si CNP-ul nu corespund!");

    const sql_select = "SELECT COUNT(*) FROM  abonament WHERE email='"+email+"'";
    query = connection.query(sql_select, function (err, result) {
      if (err) throw err;
      if (result[0]['COUNT(*)'] !== 0) {
        //console.log("Email deja existent in baza de date!");
        error.push("Email deja existent in baza de date!");
      }
    });
    query.on('end', function() {
      if (error.length === 0) {
        console.log(error);
        const sql = "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,sex) VALUES('" + nume +"','" + prenume +"','" + telefon +"','" + email +"','" + facebook +"','" + tipAbonament +"','" + nrCard +"','" + cvv +"','" + varsta + "','" + cnp + "','" + sex + "')";
        connection.query(sql, function (err, result) {
          if (err) throw err;
          console.log("Abonament achizitionat!");
        });
  
        res.status(200).send({
          message: "Abonament achizitionat!"
        });
        console.log(sql);
      } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
      }
      error=[];
    });
});